import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';

import LEAFLET from '@salesforce/resourceUrl/leaflet';

/* L is the Leaflet object constructed by the leaflet.js script */
/*global L*/

export default class Map extends LightningElement {

    @api leafletInitialized = false;

    renderedCallback() {
        if (this.leafletInitialized) {
            return;
        }
        this.leafletInitialized = true;

        Promise.all([
            loadScript(this, LEAFLET + '/leaflet.js'),
            loadStyle(this, LEAFLET + '/leaflet.css')
        ])
            .then(() => {
                this.initializeleaflet();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading Leaflet',
                        message: error.message,
                        variant: 'error'
                    })
                );
            });
    }

    initializeleaflet() {
        const mapRoot = this.template.querySelector(".map-root");
        const map = L.map(mapRoot).setView([-19.3, 46.7], 6);
        const mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(map);

    }
}